#![feature(lang_items)]
#![feature(const_fn)]
#![no_std]
#![no_main]

#[macro_use]
extern crate lazy_static;
extern crate rlibc;
extern crate spin;
extern crate volatile;

#[macro_use]
mod vga_buffer;

#[lang = "panic_fmt"] // define a function that should be called on panic
#[no_mangle]
pub extern fn rust_begin_panic(_msg: core::fmt::Arguments,
    _file: &'static str, _line: u32, _column: u32) -> !
{
    loop {}
}

const LONG_STR: &str = "012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789"; 

#[no_mangle]
pub extern fn _start() -> ! {
    println!("Hello, world{}", '!');
    loop {}
}
